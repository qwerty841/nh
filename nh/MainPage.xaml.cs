﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Thiccness = Windows.UI.Xaml.Thickness;

namespace nh
{
    public sealed partial class MainPage : Page
    {
        public static string appName = "New Horizons Catalog";
        public static string appVer = "0.0.2";
        public static bool _dbg = false, _hld = false, _rst = true;
        public static byte[] cla = { 0, 0, 0 }, clb = { 0, 0, 0 };
        public static byte[] clc = { 0, 0, 0 }, cld = { 0, 0, 0 };
        public static byte[] cle = { 0, 0, 0 }, clf = { 0, 0, 0 };
        public static object oooF, oooH, oooP, oooT;
        public static List<Item> items = new List<Item>();
        public static List<string[]> plc = new List<string[]>();
        public static StorageFolder docBase = Windows.ApplicationModel.Package.Current.InstalledLocation;
        public static StorageFile aF;
        public static Color _back, _backF, _backH, _fore, _foreF, _foreH;
        public static Button dbg, rst, hld, loadB, saveB, col;

        public enum curr:byte
        {
            miles,
            bells
        };

        public class price
        {
            int _v;
            curr _c;

            public price()
            {
                _v = 0;
                _c = curr.bells;
            }

            public price(int val, curr cur)
            {
                _v = val;
                _c = cur;
            }

            public int Value
            {
                get { return _v; }
                set { _v = value; }
            }

            public curr Curr
            {
                get { return _c; }
                set { _c = value; }
            }
        }

        public class Item
        {
            int _sk;
            int[] _sz;
            string _c, _n, _w;
            List<string> _st, _sy, _tg, _th;
            List<string> _cl;               //color
            price _b, _s;

            public Item()
            {
                _sk = 1; _sz = new int[] { 1, 1 };
                _c = ""; _n = ""; _w = "";
                _st = new List<string>(); _sy = new List<string>(); _tg = new List<string>(); _th = new List<string>();
                _b = new price(); _s = new price();
            }

            public Item(string name)
            {
                _sk = 1; _sz = new int[] { 1, 1 };
                _c = ""; _n = name; _w = "";
                _st = new List<string>(); _sy = new List<string>(); _tg = new List<string>(); _th = new List<string>();
                _b = new price(); _s = new price();
            }

            public string Name
            {
                get { return _n; }
                set { _n = value; }
            }

            public string Cat
            {
                get { return _c; }
                set { _c = value; }
            }

            public price Buy
            {
                get { return _b; }
                set { _b = value; }
            }

            public price Sell
            {
                get { return _s; }
                set { _s = value; }
            }

            public string Source
            {
                get { return _w; }
                set { _w = value; }
            }

            public List<string> Tag
            {
                get { return _tg; }
                set { _tg = value; }
            }

            public List<string> Set
            {
                get { return _st; }
                set { _st = value; }
            }

            public List<string> Theme
            {
                get { return _th; }
                set { _th = value; }
            }

            public List<string> Hue
            {
                get { return _cl; }
                set { _cl = value; }
            }

            public List<string> Style
            {
                get { return _sy; }
                set { _sy = value; }
            }

            public int Stack
            {
                get { return _sk; }
                set { _sk = value; }
            }

            public int[] Size
            {
                get { return _sz; }
                set { _sz = value; }
            }

            //season (?)
        }

        public async Task<bool> load()
        {
            ver.Text = appName + " version " + appVer;
            abt.ShowAt(this);
            await loadPref(true);
            await loadItem(true);
            tbq.GettingFocus += (ss, e) => { tbq.SelectionStart = tbq.Text.Length; tbq.SelectionLength = 0; };
            clr.Opening += async (s, e) => { await clrUpdate(); };
            foreach (object ss in clrG.Children)
            {
                if (ss is StackPanel)
                {
                    foreach (object tt in ((StackPanel)ss).Children)
                    {
                        if (tt is TextBox)
                        {
                            ((TextBox)tt).TextChanged += async (s, e) => { await clrChg(s, e); };
                            ((TextBox)tt).GettingFocus += async (s, ee) => { ((TextBox)tt).SelectionStart = ((TextBox)tt).Text.Length; ((TextBox)tt).SelectionLength = 0; };
                            ((TextBox)tt).LosingFocus += async (s, ee) =>
                            {
                                await CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                                {
                                    _back = Color.FromArgb(255, cla[0], cla[1], cla[2]);
                                    _fore = Color.FromArgb(255, clb[0], clb[1], clb[2]);
                                    _backF = Color.FromArgb(255, clc[0], clc[1], clc[2]);
                                    _foreF = Color.FromArgb(255, cld[0], cld[1], cld[2]);
                                    _backH = Color.FromArgb(255, cle[0], cle[1], cle[2]);
                                    _foreH = Color.FromArgb(255, clf[0], clf[1], clf[2]);
                                });
                                backR.Text = cla[0].ToString(); backG.Text = cla[1].ToString(); backB.Text = cla[2].ToString();
                                foreR.Text = clb[0].ToString(); foreG.Text = clb[1].ToString(); foreB.Text = clb[2].ToString();
                                backfR.Text = clc[0].ToString(); backfG.Text = clc[1].ToString(); backfB.Text = clc[2].ToString();
                                forefR.Text = cld[0].ToString(); forefG.Text = cld[1].ToString(); forefB.Text = cld[2].ToString();
                                backhR.Text = cle[0].ToString(); backhG.Text = cle[1].ToString(); backhB.Text = cle[2].ToString();
                                forehR.Text = clf[0].ToString(); forehG.Text = clf[1].ToString(); forehB.Text = clf[2].ToString();
                                await clrUpdate();
                                await updatePref();
                            };
                        }
                    }
                }
            }
            return true;
        }

        public async Task<bool> loadItem(bool frst)
        {
            if (frst)
            {
                plc.Add(new string[] { "redd", "Jolly Redd's Treasure Trawler" });
                plc.Add(new string[] { "cranny", "Nook's Cranny" });
                plc.Add(new string[] { "nook", "Nook Shopping" });
                plc.Add(new string[] { "seas", "Nook Shopping Seasonal" });
                plc.Add(new string[] { "promo", "Nook Shopping Promotion" });
                plc.Add(new string[] { "able", "Able Sisters" });
                plc.Add(new string[] { "craft", "Crafting" });
                plc.Add(new string[] { "nin", "Nintendo" });
                plc.Add(new string[] { "flick", "Flick" });
                plc.Add(new string[] { "fish", "Fishing tourney" });
                try
                {
                    aF = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/item.dat"));
                }
                catch (Exception ex)
                {
                    try
                    {
                        var picker = new FileOpenPicker();
                        picker.FileTypeFilter.Add(".dat");
                        aF = await picker.PickSingleFileAsync();
                    }
                    catch (Exception ex_)
                    {
                        await (new MessageDialog(ex_.StackTrace + ":\n" + ex_.Message)).ShowAsync();
                        return false;
                    }
                }
            }
            else
            {
                try
                {
                    var picker = new FileOpenPicker();
                    picker.FileTypeFilter.Add(".dat");
                    aF = await picker.PickSingleFileAsync();
                }
                catch (Exception ex_)
                {
                    await (new MessageDialog(ex_.StackTrace + ":\n" + ex_.Message)).ShowAsync();
                    return false;
                }
            }
            try
            {
                var prrf = await FileIO.ReadLinesAsync(aF);
                foreach (string p in prrf)
                {
                    await CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                    {
                        string[] pp = p.Split('>');
                        foreach (string ppp in pp)
                        {
                            string[] pppp = ppp.Split('<');

                            if (pppp[0] == "item")
                            {
                                string[] ppppp = pppp[1].Split(';');
                                foreach (string pppppp in ppppp)
                                {
                                    string[] ppppppp = pppppp.Split(':');
                                    string[] pppppppp = ppppppp[1].Split(',');
                                    if (ppppppp[0].Equals("name"))
                                    {
                                        if (pppppppp[0].Equals(""))
                                            break;
                                        //Debug.WriteLine(pppppppp[0]);
                                        items.Add(new Item(pppppppp[0]));
                                    }

                                    if (ppppppp[0].Equals("cat"))
                                    {
                                        items[items.Count - 1].Cat = pppppppp[0];
                                    }

                                    if (ppppppp[0].Equals("buy"))
                                    {
                                        char[] tc = { 'b', 'm' };
                                        int pr = 0;
                                        string prl = pppppppp[0].TrimEnd(tc);
                                        char prr = pppppppp[0].Last();
                                        Int32.TryParse(prl, out pr);
                                        if(prr.Equals('b'))
                                            items[items.Count - 1].Buy = new price(pr, curr.bells);
                                        if (prr.Equals('m'))
                                            items[items.Count - 1].Buy = new price(pr, curr.miles);
                                    }

                                    if (ppppppp[0].Equals("sell"))
                                    {
                                        char[] tc = { 'b', 'm' };
                                        int pr = 0;
                                        string prl = pppppppp[0].TrimEnd(tc);
                                        char prr = pppppppp[0].Last();
                                        Int32.TryParse(prl, out pr);
                                        if (prr.Equals('b'))
                                            items[items.Count - 1].Sell = new price(pr, curr.bells);
                                        if (prr.Equals('m'))
                                            items[items.Count - 1].Sell = new price(pr, curr.miles);
                                    }

                                    if (ppppppp[0].Equals("source"))
                                    {
                                        items[items.Count - 1].Source = pppppppp[0];
                                        for (int i = 0; i < plc.Count; i++)
                                        {
                                            if (plc[i][0] == (pppppppp[0]))
                                                items[items.Count - 1].Source = plc[i][1];
                                        }
                                    }

                                    if (ppppppp[0].Equals("tag"))
                                    {
                                        List<string> tt = new List<string> { };
                                        foreach (string ss in pppppppp)
                                            tt.Add(ss);
                                        items[items.Count - 1].Tag = tt;
                                    }

                                    if (ppppppp[0].Equals("set"))
                                    {
                                        List<string> tt = new List<string> { };
                                        foreach (string ss in pppppppp)
                                            tt.Add(ss);
                                        items[items.Count - 1].Set = tt;
                                    }

                                    if (ppppppp[0].Equals("theme"))
                                    {
                                        List<string> tt = new List<string> { };
                                        foreach (string ss in pppppppp)
                                            tt.Add(ss);
                                        items[items.Count - 1].Theme = tt;
                                    }

                                    if (ppppppp[0].Equals("color"))
                                    {
                                        List<string> tt = new List<string> { };
                                        foreach (string ss in pppppppp)
                                            tt.Add(ss);
                                        items[items.Count - 1].Hue = tt;
                                    }

                                    if (ppppppp[0].Equals("style"))
                                    {
                                        List<string> tt = new List<string> { };
                                        foreach (string ss in pppppppp)
                                            tt.Add(ss);
                                        items[items.Count - 1].Style = tt;
                                    }

                                    if (ppppppp[0].Equals("stack"))
                                    {
                                        int ss = 1;
                                        Int32.TryParse(pppppppp[0], out ss);
                                        items[items.Count - 1].Stack = ss;
                                    }

                                    if (ppppppp[0].Equals("size"))
                                    {
                                        int h = 1, w = 1;
                                        Int32.TryParse(pppppppp[0], out w);
                                        Int32.TryParse(pppppppp[1], out h);
                                        items[items.Count - 1].Size = new int[] { w, h };
                                    }

                                    if (ppppppp[0].Equals("season"))
                                    {
                                    }
                                }
                            }
                        }
                    });
                }
                return true;
            }
            catch (FileNotFoundException ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
            catch (Exception ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
        }

        public async Task<bool> loadPref(bool frst)
        {
            if (frst)
            {
                try
                {
                    aF = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/pref.dat"));
                }
                catch (Exception ex)
                {
                    try
                    {
                        var picker = new FileOpenPicker();
                        picker.FileTypeFilter.Add(".dat");
                        aF = await picker.PickSingleFileAsync();
                    }
                    catch (Exception ex_)
                    {
                        await (new MessageDialog(ex_.StackTrace + ":\n" + ex_.Message)).ShowAsync();
                        return false;
                    }
                }
            }
            else
            {
                try
                {
                    var picker = new FileOpenPicker();
                    picker.FileTypeFilter.Add(".dat");
                    aF = await picker.PickSingleFileAsync();
                }
                catch (Exception ex_)
                {
                    await (new MessageDialog(ex_.StackTrace + ":\n" + ex_.Message)).ShowAsync();
                    return false;
                }
            }
            try
            {
                var prrf = await FileIO.ReadLinesAsync(aF);
                foreach (string p in prrf)
                {
                    await CoreWindow.GetForCurrentThread().Dispatcher.RunAsync(CoreDispatcherPriority.High, () =>
                    {
                        string[] pp = p.Split('>');
                        foreach (string ppp in pp)
                        {
                            string[] pppp = ppp.Split('<');
                            if (pppp[0] == "debug")
                            {
                                if (pppp[1].Equals("on"))
                                    _dbg = true;
                                if (pppp[1].Equals("off"))
                                    _dbg = false;
                            }

                            if (pppp[0] == "reset")
                            {
                                if (pppp[1].Equals("on"))
                                    _rst = true;
                                if (pppp[1].Equals("off"))
                                    _rst = false;
                            }

                            if (pppp[0] == "hold")
                            {
                                if (pppp[1].Equals("on"))
                                    _hld = true;
                                if (pppp[1].Equals("off"))
                                    _hld = false;
                            }

                            if (pppp[0] == "color")
                            {
                                string[] ppppp = pppp[1].Split(';');
                                foreach (string pppppp in ppppp)
                                {
                                    string[] ppppppp = pppppp.Split(':');
                                    string[] pppppppp = ppppppp[1].Split(',');
                                    if (ppppppp[0].Equals("background"))
                                    {
                                        cla[0] = byte.Parse(pppppppp[0]);
                                        cla[1] = byte.Parse(pppppppp[1]);
                                        cla[2] = byte.Parse(pppppppp[2]);
                                        _back = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                    if (ppppppp[0].Equals("foreground"))
                                    {
                                        clb[0] = byte.Parse(pppppppp[0]);
                                        clb[1] = byte.Parse(pppppppp[1]);
                                        clb[2] = byte.Parse(pppppppp[2]);
                                        _fore = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                    if (ppppppp[0].Equals("backfocus"))
                                    {
                                        clc[0] = byte.Parse(pppppppp[0]);
                                        clc[1] = byte.Parse(pppppppp[1]);
                                        clc[2] = byte.Parse(pppppppp[2]);
                                        _backF = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                    if (ppppppp[0].Equals("forefocus"))
                                    {
                                        cld[0] = byte.Parse(pppppppp[0]);
                                        cld[1] = byte.Parse(pppppppp[1]);
                                        cld[2] = byte.Parse(pppppppp[2]);
                                        _foreF = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                    if (ppppppp[0].Equals("backhover"))
                                    {
                                        cle[0] = byte.Parse(pppppppp[0]);
                                        cle[1] = byte.Parse(pppppppp[1]);
                                        cle[2] = byte.Parse(pppppppp[2]);
                                        _backH = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                    if (ppppppp[0].Equals("forehover"))
                                    {
                                        clf[0] = byte.Parse(pppppppp[0]);
                                        clf[1] = byte.Parse(pppppppp[1]);
                                        clf[2] = byte.Parse(pppppppp[2]);
                                        _foreH = Color.FromArgb(255, byte.Parse(pppppppp[0]), byte.Parse(pppppppp[1]),
                                            byte.Parse(pppppppp[2]));
                                    }
                                }
                            }
                        }
                    });
                }
                return true;
            }
            catch (FileNotFoundException ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
            catch (Exception ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
        }

        public async Task<bool> savePref()
        {
            try
            {
                var picker = new FileSavePicker();
                picker.FileTypeChoices.Add(".dat", new List<string>() { ".dat" });
                aF = await picker.PickSaveFileAsync();
            }
            catch (Exception ex_)
            {
                await (new MessageDialog(ex_.StackTrace + ":\n" + ex_.Message)).ShowAsync();
                return false;
            }
            try
            {
                var stream = await aF.OpenAsync(FileAccessMode.ReadWrite);
                using (var ostream = stream.GetOutputStreamAt(0))
                {
                    using (var dwriter = new Windows.Storage.Streams.DataWriter(ostream))
                    {
                        dwriter.WriteString("color<");
                        dwriter.WriteString("background:");
                        dwriter.WriteString(cla[0].ToString() + ",");
                        dwriter.WriteString(cla[1].ToString() + ",");
                        dwriter.WriteString(cla[2].ToString() + ";");
                        dwriter.WriteString("foreground:");
                        dwriter.WriteString(clb[0].ToString() + ",");
                        dwriter.WriteString(clb[1].ToString() + ",");
                        dwriter.WriteString(clb[2].ToString() + ";");
                        dwriter.WriteString("backfocus:");
                        dwriter.WriteString(clc[0].ToString() + ",");
                        dwriter.WriteString(clc[1].ToString() + ",");
                        dwriter.WriteString(clc[2].ToString() + ";");
                        dwriter.WriteString("forefocus:");
                        dwriter.WriteString(cld[0].ToString() + ",");
                        dwriter.WriteString(cld[1].ToString() + ",");
                        dwriter.WriteString(cld[2].ToString() + ";");
                        dwriter.WriteString("backhover:");
                        dwriter.WriteString(cle[0].ToString() + ",");
                        dwriter.WriteString(cle[1].ToString() + ",");
                        dwriter.WriteString(cle[2].ToString() + ";");
                        dwriter.WriteString("forehover:");
                        dwriter.WriteString(clf[0].ToString() + ",");
                        dwriter.WriteString(clf[1].ToString() + ",");
                        dwriter.WriteString(clf[2].ToString());
                        dwriter.WriteString(">\n");
                        dwriter.WriteString("debug<");
                        if (_dbg)
                            dwriter.WriteString("on");
                        else
                            dwriter.WriteString("off");
                        dwriter.WriteString(">\n");
                        dwriter.WriteString("reset<");
                        if (_rst)
                            dwriter.WriteString("on");
                        else
                            dwriter.WriteString("off");
                        dwriter.WriteString(">\n");
                        dwriter.WriteString("hold<");
                        if (_hld)
                            dwriter.WriteString("on");
                        else
                            dwriter.WriteString("off");
                        dwriter.WriteString(">\n");
                        await dwriter.StoreAsync();
                        await dwriter.FlushAsync();
                    }
                }
                stream.Dispose();
                return true;
            }
            catch (FileNotFoundException ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
            catch (Exception ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
        }

        private async Task<bool> updatePref()
        {
            Style stlF = new Style(typeof(FlyoutPresenter));
            stlF.Setters.Add(new Setter(FlyoutPresenter.BackgroundProperty, new SolidColorBrush(_back)));
            abt.FlyoutPresenterStyle = stlF;
            inp.FlyoutPresenterStyle = stlF;
            clr.FlyoutPresenterStyle = stlF;
            Background = new SolidColorBrush(_back);
            tbl.Foreground = new SolidColorBrush(_fore);
            Style ss = new Style { BasedOn = (Style)Application.Current.Resources["Textbox"], TargetType = typeof(TextBox) };
            await replaceStyle(ss, ForegroundProperty, _fore);
            await replaceStyle(ss, BackgroundProperty, _back);
            await replaceStyle(ss, BorderBrushProperty, _fore);
            Style sb = new Style { BasedOn = (Style)Application.Current.Resources["Button"], TargetType = typeof(Button) };
            await replaceStyle(sb, ForegroundProperty, _fore);
            await replaceStyle(sb, BackgroundProperty, _back);
            await replaceStyle(sb, BorderBrushProperty, _fore);
            for (int i = 0; i < ((StackPanel)abt.Content).Children.Count; i++)
                if (((StackPanel)abt.Content).Children[i] is TextBlock)
                    ((TextBlock)(((StackPanel)abt.Content).Children[i])).Foreground = new SolidColorBrush(_fore);
            for (int i = 0; i < clrG.Children.Count; i++)
            {
                if (clrG.Children[i] is TextBlock)
                    ((TextBlock)(clrG.Children[i])).Foreground = new SolidColorBrush(_fore);
                if (clrG.Children[i] is StackPanel)
                {
                    StackPanel sp = (StackPanel)(clrG.Children[i]);
                    for (int j = 0; j < sp.Children.Count; j++)
                    {
                        if (sp.Children[j] is TextBox)
                        {
                            ((TextBox)(sp.Children[j])).Style = ss;
                            ((TextBox)(sp.Children[j])).PointerEntered += PointerOver;
                            ((TextBox)(sp.Children[j])).PointerExited += PointerOut;
                            ((TextBox)(sp.Children[j])).GotFocus += Focus;
                            ((TextBox)(sp.Children[j])).LostFocus += UnFocus;
                        }
                    }
                }
            }
            for (int i = 0; i < prf.Children.Count; i++)
            {
                if (prf.Children[i] is Button)
                {
                    ((Button)(prf.Children[i])).Style = sb;
                    ((Button)(prf.Children[i])).PointerEntered += PointerOver;
                    ((Button)(prf.Children[i])).PointerExited += PointerOut;
                    ((Button)(prf.Children[i])).GotFocus += Focus;
                    ((Button)(prf.Children[i])).LostFocus += UnFocus;
                }
            }
            if (strt.Children[0] is StackPanel)
            {
                StackPanel sp = (StackPanel)strt.Children[0];
                if (sp.Children[0] is Grid)
                {
                    Grid gr = (Grid)sp.Children[0];
                    for (int i = 0; i < gr.Children.Count; i++)
                    {
                        if (gr.Children[i] is TextBlock)
                        {
                            ((TextBlock)gr.Children[i]).Foreground = new SolidColorBrush(_fore);
                        }
                        if (gr.Children[i] is ScrollViewer)
                        {
                            ScrollViewer sv = (ScrollViewer)gr.Children[i];
                            if (sv.Content is StackPanel)
                            {
                                StackPanel spp = (StackPanel)sv.Content;
                                foreach (object oo in spp.Children)
                                {
                                    if (oo is Border)
                                    {
                                        Border bd = (Border)oo;
                                        if (bd.Child is StackPanel)
                                        {
                                            StackPanel sppp = (StackPanel)bd.Child;
                                            for (int j = 0; j < sppp.Children.Count; j++)
                                            {
                                                if (sppp.Children[j] is TextBlock)
                                                {
                                                    ((TextBlock)sppp.Children[j]).Foreground = new SolidColorBrush(_fore);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            tbq.Style = ss;
            tbq.PointerEntered += PointerOver;
            tbq.PointerExited += PointerOut;
            tbq.GotFocus += Focus;
            tbq.LostFocus += UnFocus;
            if (_dbg)
                dbg.FontWeight = FontWeights.Bold;
            else
                dbg.FontWeight = FontWeights.Normal;
            if (_rst)
                rst.FontWeight = FontWeights.Bold;
            else
                rst.FontWeight = FontWeights.Normal;
            if (_hld)
                hld.Content = "HOLD";
            else
                hld.Content = "TAP";
            if (_dbg)
                ssss.Visibility = Visibility.Visible;
            return true;
        }

        public async Task<bool> update()
        {
            try
            {
            }
            catch (Exception ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
            return true;
        }

        public async Task<bool> clrUpdate()
        {
            backC.Fill = new SolidColorBrush(_back);
            foreC.Fill = new SolidColorBrush(_fore);
            backfC.Fill = new SolidColorBrush(_backF);
            forefC.Fill = new SolidColorBrush(_foreF);
            backhC.Fill = new SolidColorBrush(_backH);
            forehC.Fill = new SolidColorBrush(_foreH);
            return true;
        }

        public async Task<bool> clrChg(object sender, TextChangedEventArgs e)
        {
            byte g = 0;
            bool s = false;
            if (sender.Equals(backR))
            {
                s = byte.TryParse(backR.Text, out g);
                if (s)
                    cla[0] = g;
            }
            if (sender.Equals(backG))
            {
                s = byte.TryParse(backG.Text, out g);
                if (s)
                    cla[1] = g;
            }
            if (sender.Equals(backB))
            {
                s = byte.TryParse(backB.Text, out g);
                if (s)
                    cla[2] = g;
            }
            if (sender.Equals(foreR))
            {
                s = byte.TryParse(foreR.Text, out g);
                if (s)
                    clb[0] = g;
            }
            if (sender.Equals(foreG))
            {
                s = byte.TryParse(foreG.Text, out g);
                if (s)
                    clb[1] = g;
            }
            if (sender.Equals(foreB))
            {
                s = byte.TryParse(foreB.Text, out g);
                if (s)
                    clb[2] = g;
            }
            if (sender.Equals(backfR))
            {
                s = byte.TryParse(backfR.Text, out g);
                if (s)
                    clc[0] = g;
            }
            if (sender.Equals(backfG))
            {
                s = byte.TryParse(backfG.Text, out g);
                if (s)
                    clc[1] = g;
            }
            if (sender.Equals(backfB))
            {
                s = byte.TryParse(backfB.Text, out g);
                if (s)
                    clc[2] = g;
            }
            if (sender.Equals(forefR))
            {
                s = byte.TryParse(forefR.Text, out g);
                if (s)
                    cld[0] = g;
            }
            if (sender.Equals(forefG))
            {
                s = byte.TryParse(forefG.Text, out g);
                if (s)
                    cld[1] = g;
            }
            if (sender.Equals(forefB))
            {
                s = byte.TryParse(forefB.Text, out g);
                if (s)
                    cld[2] = g;
            }
            if (sender.Equals(backhR))
            {
                s = byte.TryParse(backhR.Text, out g);
                if (s)
                    cle[0] = g;
            }
            if (sender.Equals(backhG))
            {
                s = byte.TryParse(backhG.Text, out g);
                if (s)
                    cle[1] = g;
            }
            if (sender.Equals(backhB))
            {
                s = byte.TryParse(backhB.Text, out g);
                if (s)
                    cle[2] = g;
            }
            if (sender.Equals(forehR))
            {
                s = byte.TryParse(forehR.Text, out g);
                if (s)
                    clf[0] = g;
            }
            if (sender.Equals(forehG))
            {
                s = byte.TryParse(forehG.Text, out g);
                if (s)
                    clf[1] = g;
            }
            if (sender.Equals(forehB))
            {
                s = byte.TryParse(forehB.Text, out g);
                if (s)
                    clf[2] = g;
            }
            backR.Text = cla[0].ToString(); backG.Text = cla[1].ToString(); backB.Text = cla[2].ToString();
            foreR.Text = clb[0].ToString(); foreG.Text = clb[1].ToString(); foreB.Text = clb[2].ToString();
            backfR.Text = clc[0].ToString(); backfG.Text = clc[1].ToString(); backfB.Text = clc[2].ToString();
            forefR.Text = cld[0].ToString(); forefG.Text = cld[1].ToString(); forefB.Text = cld[2].ToString();
            backhR.Text = cle[0].ToString(); backhG.Text = cle[1].ToString(); backhB.Text = cle[2].ToString();
            forehR.Text = clf[0].ToString(); forehG.Text = clf[1].ToString(); forehB.Text = clf[2].ToString();
            return true;
        }

        public async Task<bool> replaceStyle(Style ss, DependencyProperty dp, object val)
        {
            try
            {
                for (int i = ss.Setters.Count - 1; i >= 0; i--)
                    if ((ss.Setters[i] as Setter).Property.Equals(dp))
                        ss.Setters.RemoveAt(i);
                ss.Setters.Add(new Setter { Property = dp, Value = val });
                return true;
            }
            catch (Exception ex)
            {
                try
                {
                    Style SS = new Style { BasedOn = ss };
                    for (int i = SS.Setters.Count - 1; i >= 0; i--)
                        if ((SS.Setters[i] as Setter).Property.Equals(dp))
                            SS.Setters.RemoveAt(i);
                    SS.Setters.Add(new Setter { Property = dp, Value = val });
                    return true;
                }
                catch (Exception ex_)
                {
                    return false;
                }
            }
        }

        public async void PointerOver(object sender, PointerRoutedEventArgs e)
        {
            oooP = sender;
            if (sender is TextBox)
            {
                TextBox tb = (TextBox)sender;
                var g = ((TextBox)sender).Parent;
                Style ss = new Style { BasedOn = tb.Style, TargetType = typeof(TextBox) };
                await replaceStyle(ss, ForegroundProperty, _foreH);
                await replaceStyle(ss, BackgroundProperty, _backH);
                await replaceStyle(ss, BorderBrushProperty, Colors.Aqua);
                ((TextBox)sender).Style = ss;
            }
            if (sender is Button)
            {
                Button bt = (Button)sender;
                var g = ((Button)sender).Parent;
                Style ss = new Style { BasedOn = bt.Style, TargetType = typeof(Button) };
                await replaceStyle(ss, ForegroundProperty, _foreH);
                await replaceStyle(ss, BackgroundProperty, _backH);
                await replaceStyle(ss, BorderBrushProperty, Colors.Aqua);
                ((Button)sender).Style = ss;
            }
        }

        public async void PointerOut(object sender, PointerRoutedEventArgs e)
        {
            oooP = sender;
            if (sender is TextBox)
            {
                TextBox tb = (TextBox)sender;
                var g = ((TextBox)sender).Parent;
                Style ss = new Style { BasedOn = tb.Style, TargetType = typeof(TextBox) };
                FocusState FS = ((TextBox)sender).FocusState;
                if ((FS.Equals(FocusState.Pointer)) || (FS.Equals(FocusState.Keyboard)) || (FS.Equals(FocusState.Programmatic)))
                {
                    await replaceStyle(ss, ForegroundProperty, _foreF);
                    await replaceStyle(ss, BackgroundProperty, _backF);
                    await replaceStyle(ss, BorderBrushProperty, _foreF);
                }
                else
                {
                    await replaceStyle(ss, ForegroundProperty, _fore);
                    await replaceStyle(ss, BackgroundProperty, _back);
                    await replaceStyle(ss, BorderBrushProperty, _fore);
                }
                ((TextBox)sender).Style = ss;
            }
            if (sender is Button)
            {
                Button bt = (Button)sender;
                var g = ((Button)sender).Parent;
                Style ss = new Style { BasedOn = bt.Style, TargetType = typeof(Button) };
                FocusState FS = ((Button)sender).FocusState;
                if ((FS.Equals(FocusState.Pointer)) || (FS.Equals(FocusState.Keyboard)) || (FS.Equals(FocusState.Programmatic)))
                {
                    await replaceStyle(ss, ForegroundProperty, _foreF);
                    await replaceStyle(ss, BackgroundProperty, _backF);
                    await replaceStyle(ss, BorderBrushProperty, _foreF);
                }
                else
                {
                    await replaceStyle(ss, ForegroundProperty, _fore);
                    await replaceStyle(ss, BackgroundProperty, _back);
                    await replaceStyle(ss, BorderBrushProperty, _fore);
                }
                ((Button)sender).Style = ss;
            }
        }

        public async void Focus(object sender, RoutedEventArgs e)
        {
            oooF = sender;
            if (sender is TextBox)
            {
                TextBox tb = (TextBox)sender;
                var g = ((TextBox)sender).Parent;
                Style ss = new Style { BasedOn = tb.Style, TargetType = typeof(TextBox) };
                await replaceStyle(ss, ForegroundProperty, _foreF);
                await replaceStyle(ss, BackgroundProperty, _backF);
                await replaceStyle(ss, BorderBrushProperty, _foreF);
                ((TextBox)sender).Style = ss;
            }
            if (sender is Button)
            {
                Button bt = (Button)sender;
                var g = ((Button)sender).Parent;
                Style ss = new Style { BasedOn = bt.Style, TargetType = typeof(Button) };
                await replaceStyle(ss, ForegroundProperty, _foreF);
                await replaceStyle(ss, BackgroundProperty, _backF);
                await replaceStyle(ss, BorderBrushProperty, _foreF);
                ((Button)sender).Style = ss;
            }
        }

        public async void UnFocus(object sender, RoutedEventArgs e)
        {
            oooF = sender;
            if (sender is TextBox)
            {
                TextBox tb = (TextBox)sender;
                var g = ((TextBox)sender).Parent;
                Style ss = new Style { BasedOn = tb.Style, TargetType = typeof(TextBox) };
                await replaceStyle(ss, ForegroundProperty, _fore);
                await replaceStyle(ss, BackgroundProperty, _back);
                await replaceStyle(ss, BorderBrushProperty, _fore);
                ((TextBox)sender).Style = ss;
            }
            if (sender is Button)
            {
                Button bt = (Button)sender;
                var g = ((Button)sender).Parent;
                Style ss = new Style { BasedOn = bt.Style, TargetType = typeof(Button) };
                await replaceStyle(ss, ForegroundProperty, _fore);
                await replaceStyle(ss, BackgroundProperty, _back);
                await replaceStyle(ss, BorderBrushProperty, _fore);
                ((Button)sender).Style = ss;
            }
        }

        public async void Tap(object sender, TappedRoutedEventArgs e)
        {
            if (sender is TextBlock)
            {
                TextBlock tt = (TextBlock)sender;
                var g = ((TextBlock)sender).Parent;
            }
            if (sender.Equals(visS))
            {
                if (strt.Visibility == Visibility.Collapsed)
                    strt.Visibility = Visibility.Visible;
                else
                    strt.Visibility = Visibility.Collapsed;
            }
            if (sender.Equals(visP))
            {
                if (prf.Visibility == Visibility.Collapsed)
                    prf.Visibility = Visibility.Visible;
                else
                    prf.Visibility = Visibility.Collapsed;
            }
        }

        public async void TapR(object sender, RightTappedRoutedEventArgs e)
        {
            if (_hld)
                await SecStuff(sender);
            else
                await PriStuff(sender);
        }

        public async void Hold(object sender, HoldingRoutedEventArgs e)
        {
            if (_hld)
                await PriStuff(sender);
            else
                await SecStuff(sender);
        }

        public async Task<bool> PriStuff(object sender)
        {
             if (sender is TextBlock)
             {
                 TextBlock tt = (TextBlock)sender;
                 var g = ((TextBlock)sender).Parent;
             }
            return true;
         }

        public async Task<bool> SecStuff(object sender)
         {
             if (sender is TextBlock)
             {
                 TextBlock tt = (TextBlock)sender;
                 var g = ((TextBlock)sender).Parent;
             }
            return true;
         }

        public async Task<bool> potato()
        {
            try
            {
                StackPanel sp = new StackPanel { Orientation = Orientation.Vertical };
                Grid gg = new Grid();
                //Debug.WriteLine(items.Count.ToString());
                //for (int i = 0; i < items.Count; i++)
                //{
                //    //Debug.WriteLine(items[i].Name);
                //    strt.Children.Add(new TextBlock { Text = items[i].Name, Width = 100, Height = 50 });
                //}
                for (int i = 0; i <= 5; i++)
                {
                    gg.ColumnDefinitions.Add(new ColumnDefinition());
                }
                for (int i = 0; i < items.Count; i++)
                {
                    TextBlock tn = new TextBlock { Text = items[i].Name, Margin = new Thiccness(10) };
                    TextBlock tc = new TextBlock { Text = items[i].Cat, Margin = new Thiccness(10) };
                    TextBlock tb = new TextBlock { Text = items[i].Buy.Value.ToString(), Margin = new Thiccness(10) };
                    if ((byte)(items[i].Buy.Curr) == 1)
                        tb.Text += "b";
                    else
                        tb.Text += "m";
                    TextBlock ts = new TextBlock { Text = items[i].Sell.Value.ToString(), Margin = new Thiccness(10) };
                    if ((byte)(items[i].Sell.Curr) == 1)
                        ts.Text += "b";
                    else
                        ts.Text += "m";
                    TextBlock tw = new TextBlock { Text = items[i].Source, Margin = new Thiccness(10) };
                    gg.RowDefinitions.Add(new RowDefinition());
                    Grid.SetRow(tn, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(tn, 0);
                    gg.Children.Add(tn);
                    Grid.SetRow(tc, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(tc, 1);
                    gg.Children.Add(tc);
                    Grid.SetRow(tb, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(tb, 2);
                    gg.Children.Add(tb);
                    Grid.SetRow(ts, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(ts, 3);
                    gg.Children.Add(ts);
                    Grid.SetRow(tw, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(tw, 4);
                    gg.Children.Add(tw);
                    gg.RowDefinitions.Add(new RowDefinition());
                    ScrollViewer sv = new ScrollViewer { HorizontalScrollBarVisibility = ScrollBarVisibility.Visible, VerticalScrollBarVisibility = ScrollBarVisibility.Hidden };
                    StackPanel spc = new StackPanel { Orientation = Orientation.Horizontal, /*Margin = new Thiccness(10)*/ };
                    if (!(items[i].Hue is null))
                    {
                        foreach (string st in items[i].Hue)
                        {
                            Border bd = new Border {BorderThickness = new Thiccness(2), BorderBrush = new SolidColorBrush(_fore), Margin = new Thiccness(10) };
                            spc.Children.Add(bd);
                            StackPanel spcc = new StackPanel { Orientation=Orientation.Vertical };
                            spcc.Children.Add(new CheckBox { IsChecked = false, Margin = new Thiccness(10) });
                            spcc.Children.Add(new TextBlock { Text = st, Foreground = new SolidColorBrush(_fore), Margin = new Thiccness(10) });
                            bd.Child = spcc;
                        }
                    }
                    else
                    {
                        Border bd = new Border { BorderThickness = new Thiccness(2), BorderBrush = new SolidColorBrush(_fore), Margin = new Thiccness(10) };
                        spc.Children.Add(bd);
                        StackPanel spcc = new StackPanel { Orientation = Orientation.Vertical };
                        spcc.Children.Add(new CheckBox { IsChecked = false, Margin = new Thiccness(10) });
                        spcc.Children.Add(new TextBlock { Text = "none", Margin = new Thiccness(10) });
                        bd.Child = spcc;
                    }
                    //spc.Children.Add(tnn);
                    Grid.SetRow(sv, gg.RowDefinitions.Count - 1);
                    Grid.SetColumn(sv, 0);
                    Grid.SetColumnSpan(sv, 6);
                    sv.Content = spc;
                    gg.Children.Add(sv);
                }
                sp.Children.Add(gg);
                strt.Children.Add(sp);
                //spc.Children.Add(new TextBlock { Text = "blah" });
                //strt.Children.Add(spc);
                return true;
            }
            catch (Exception ex)
            {
                await (new MessageDialog(ex.StackTrace + ":\n" + ex.Message)).ShowAsync();
                return false;
            }
        }

        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += async (s, e) => { await load(); await potato(); await updatePref(); };
            oooF = this; oooH = this; oooP = this; oooT = this;
            col = new Button { Content = "COLOR", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            dbg = new Button { Content = "DEBUG", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            rst = new Button { Content = "RESET", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            hld = new Button { Content = "TAP", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            loadB = new Button { Content = "LOAD", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            saveB = new Button { Content = "SAVE", VerticalAlignment = VerticalAlignment.Top, Margin = new Thiccness(5) };
            col.Click += async (s, e) => { await clrUpdate(); clr.ShowAt(this); };
            dbg.Click += async (s, e) => { _dbg = !_dbg; await updatePref(); };
            rst.Click += async (s, e) => { _rst = !_rst; await updatePref(); };
            hld.Click += async (s, e) => { _hld = !_hld; await updatePref(); };
            loadB.Click += async (s, e) => { await loadPref(false); await updatePref(); };
            saveB.Click += async (s, e) => { await savePref(); };
            prf.Children.Add(col);
            //prf.Children.Add(dbg);
            //prf.Children.Add(rst);
            prf.Children.Add(hld);
            prf.Children.Add(loadB);
            prf.Children.Add(saveB);
        }
    }
}
